package de.psicho.redmine.iTextile.utils;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

public class ListUtilsTest {

    private static final String INPUT_SUFFIX = ".input.html";
    private static final String OUTPUT_SUFFIX = ".output.html";

    @Test
    public void testSimple() {
        performTest("testSimple");
    }

    @Test
    public void testComplex() {
        performTest("testComplex");
    }

    private void performTest(String methodName) {
        String canonicalName = this.getClass().getCanonicalName();
        String currentPath = canonicalName.replaceAll("\\.", "/") + "/" + methodName;
        String input = ResourceUtils.readResource(currentPath + INPUT_SUFFIX);
        String expectedOutput = ResourceUtils.readResource(currentPath + OUTPUT_SUFFIX);

        String calculatedOutput = ListUtils.transformLists(input);

        assertThat(normalized(calculatedOutput), is(normalized(expectedOutput)));
    }

    private String normalized(String output) {
        return output.replace(" ", "").replace("\r", "").replace("\n", "").replace("\t", "");
    }
}
