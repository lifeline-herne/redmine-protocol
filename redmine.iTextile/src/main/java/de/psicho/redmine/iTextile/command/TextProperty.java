package de.psicho.redmine.iTextile.command;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class TextProperty {

    @Builder.Default
    private final FontFamily font = FontFamily.HELVETICA;

    @Builder.Default
    private final float size = 12.0f;

    @Builder.Default
    private final int style = Font.NORMAL;

    @Builder.Default
    private final BaseColor color = BaseColor.BLACK;

    @Builder.Default
    private final int alignment = Element.ALIGN_LEFT;

}
