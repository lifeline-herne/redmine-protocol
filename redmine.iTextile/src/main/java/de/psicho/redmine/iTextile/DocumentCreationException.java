package de.psicho.redmine.iTextile;

public class DocumentCreationException extends Exception {

    private static final long serialVersionUID = -4113329404814053184L;

    /**
     * <p>Creates a <code>DocumentCreationException</code>.
     *
     * @param ex an exception that has to be turned into a DocumentCreationException
     */
    public DocumentCreationException(Exception ex) {
        super(ex);
    }

}
