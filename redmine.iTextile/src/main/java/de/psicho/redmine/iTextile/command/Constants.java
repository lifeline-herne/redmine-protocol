package de.psicho.redmine.iTextile.command;

public class Constants {

    static final float A4_WIDTH = 523f;
    static final String STYLESHEET = "itextile.style.css";
}
