package de.psicho.redmine.iTextile.command;

import static de.psicho.redmine.iTextile.command.Constants.A4_WIDTH;
import static de.psicho.redmine.iTextile.command.Constants.STYLESHEET;

import java.io.IOException;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.tool.xml.ElementList;
import com.itextpdf.tool.xml.XMLWorkerHelper;

import de.psicho.redmine.iTextile.ProcessingException;
import de.psicho.redmine.iTextile.utils.ResourceUtils;
import net.java.textilej.parser.MarkupParser;
import net.java.textilej.parser.markup.Dialect;

public class Paragraph implements Command {

    private String text;
    private TextProperty property;
    private Dialect dialect;
    private Image image;
    private String url;

    public Paragraph(String text, TextProperty textProperty) {
        this.text = text;
        this.property = textProperty;
    }

    public Paragraph(String text, Dialect dialect) {
        this.text = text;
        this.dialect = dialect;
    }

    public Paragraph(byte[] image, String url) {
        try {
            this.image = Image.getInstance(image);
            if (this.image.getWidth() > A4_WIDTH) {
                this.image.scaleToFit(A4_WIDTH, A4_WIDTH);
            }
        } catch (BadElementException | IOException ex) {
            throw new IllegalArgumentException("Cannot convert byte array to image.", ex);
        }
        this.url = url;
    }

    @Override
    public void process(Document document) throws ProcessingException {
        if (text != null) {
            if (dialect != null) {
                processTextWithDialect(document);
            } else if (property != null) {
                processTextWithFormat(document);
            } else {
                throw new ProcessingException("Dialect and formatting not set. Provide exactly one!");
            }
        } else {
            // image != null
            processImage(document);
        }
    }

    private void processImage(Document document) {
        try {
            if (url != null) {
                Chunk cImage = new Chunk(image, 0, 0, true);
                Anchor anchor = new Anchor(cImage);
                anchor.setReference(url);
                document.add(anchor);
            } else {
                document.add(image);
            }
        } catch (DocumentException ex) {
            throw new ProcessingException(ex);
        }
    }

    private void processTextWithDialect(Document document) {
        com.itextpdf.text.Paragraph paragraph = new com.itextpdf.text.Paragraph();
        String htmlContent = new MarkupParser(dialect).parseToHtml(text);
        String css = ResourceUtils.readResource(STYLESHEET);

        try {
            ElementList list = XMLWorkerHelper.parseToElementList(htmlContent, css);
            paragraph.addAll(list);
            document.add(paragraph);
        } catch (IOException | DocumentException ex) {
            throw new ProcessingException(ex);
        }
    }

    private void processTextWithFormat(Document document) {
        Font font = new Font(property.getFont(), property.getSize(), property.getStyle(), property.getColor());
        Chunk chunk = new Chunk(text, font);
        com.itextpdf.text.Paragraph paragraph = new com.itextpdf.text.Paragraph(chunk);
        paragraph.setAlignment(property.getAlignment());
        paragraph.setSpacingAfter(8.0f);
        try {
            document.add(paragraph);
        } catch (DocumentException ex) {
            throw new ProcessingException(ex);
        }
    }

}
