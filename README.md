# redmine-protocol
Spring Boot project. Reads tickets from Redmine in Textile syntax and uses iText to create a PDF.

For more details, see redmine.protocol/README.md.
