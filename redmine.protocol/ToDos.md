## Reorganize Project

### ProtocolController

1. It has way too much dependencies (see huge number of injected fields). Separate it into many smaller responsibilities.
1. Refactor to thymeleaf
    * enable theming (Solarized Dark, Some Light)
1. Remove dependency of TextileDialect: use enum instead. Is it necessary to have:
    * one enum in ITextService (to decouple controller from redmine.iTextile)
    * and another one in de.psicho.redmine.iTextile.iTextile (to decouple Dialects from redmine.protocol)
      -> not sure about the latter one; maybe we can regard TextileDialect as generic enough
1. Migrate to Buildpacks

### ITextService

1. Move dependency of itextpdf completely out ot the project

## Add a lot of tests
