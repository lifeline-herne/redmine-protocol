# Run
## In docker (locally or remote)
`docker-compose up -d`

## From IDE
Create a profile `application-local.yml` and run the Boot application using it.

## As a jar (deprecated)
	
    java -jar redmine.protocol-2.0.0.jar
	
or with params, e.g. for running on a server:  

    java -jar -Dspring.profiles.active=remote -Dlogging.file=/home/user/redmine.protocol.log /home/user/redmine.protocol-2.0.0.jar  

_Note: a configured file `application-remote.yml` must be in the same directory as the jar._

# Installation
1. In Redmine
   1. Create tracker type for protocol and add fields for number, participants, meal, location, moderation and devotion.
   1. Create an Api Access Key for your user: Log into redmine > My Account > see right side.
1. Copy `src/main/resources/application-example.yml` to your docker-compose project and configure it.
   1. Anything else from `application.yml` can be configured. 
1. Create a `docker-compose.yml` with content like:
```
  protocol:
    environment:
      SPRING_PROFILES_ACTIVE: example
    image: registry.gitlab.com/lifeline-herne/redmine-protocol:master
    ports:
      - '8090:8090'
    restart: 'unless-stopped'
    volumes:
      - source: protocols
        target: /results
        type: volume
      - source: ./application-example.yml
        target: /application-example.yml
        type: bind
```

# Introduction
- Spring Boot Application with external DB and Spring JDBCTemplate
- Web-UI for creating and reviewing protocols based on DB state.
- When a protocol is closed via UI, the ap  
    * sets the subject
    * attaches created PDF to protocol's ticket
    * closes the protocol
    * emails the protocol with all attachments to participants 
- Project iTextile
    * Uses iText
    * Uses engine of textile-j (https://svn.java.net/svn/textile-j~svn/), based on regular expressions
    * additionally converts #123 into a link
- Rendering
    * Renderer for Wiki-Syntax
    * line breaks, paragraphs
    * tables
    
# Usage in Issues
* Write names of participants in same notation as configured in section `redmine.protocol.members` of `application.yml`
* Add attachment like `attachment:"filename.ext"`

# Sources
## API for Redmine
- http://www.redmine.org/projects/redmine/wiki/Rest_api_with_java
- http://www.redmine.org/projects/redmine/wiki/Rest_api
- https://github.com/taskadapter/redmine-java-api/blob/master/README.md

## Textile
https://txstyle.org/article/34/textile-resources

# Comparison of PDF Frameworks 
- PDFBox (GPL)
    * https://github.com/dhorions/boxable
    * https://issues.apache.org/jira/browse/PDFBOX-2618
    * https://pdfbox.apache.org/index.html
    Conclusion:
    * Low-Level-API
    * Tables only with external libraries (boxable)
    * no support for paragraphs
- iText (AGPL, like OTRS)
    * http://developers.itextpdf.com/examples/itext-building-blocks/list-examples
    Conclusion:
    * High level API
    * tables
    * paragraphs
    * bold, colors, lists (additionally with self defined bullets), justification
