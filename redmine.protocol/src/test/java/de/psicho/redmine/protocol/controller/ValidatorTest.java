package de.psicho.redmine.protocol.controller;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import de.psicho.redmine.protocol.api.IssueHandler;
import de.psicho.redmine.protocol.config.Fields;
import de.psicho.redmine.protocol.config.Protocol;
import de.psicho.redmine.protocol.config.RedmineConfigurer;
import de.psicho.redmine.protocol.utils.LinkUtils;

@ExtendWith(MockitoExtension.class)
public class ValidatorTest {

    @Mock
    private IssueHandler issueHandler;

    @Mock
    private LinkUtils linkUtils;

    @Test
    public void get_field_returns_name() {
        // given
        Validator validator = createValidatorWithDevotion("Andacht");

        // when
        final var result = validator.getProtocolFieldByName("devotion");

        // then
        assertThat(result).isEqualTo("Andacht");
    }

    @Test
    public void get_non_existing_protocol_field_returns_null() {
        // given
        Validator validator = createValidatorWithDevotion("Andacht");

        // when
        final var result = validator.getProtocolFieldByName("wrongName");

        // then
        assertThat(result).isNull();
    }

    private Validator createValidatorWithDevotion(String value) {
        final var fields = new Fields();
        fields.setDevotion(value);
        final var protocol = new Protocol();
        protocol.setFields(fields);
        final var redmine = new RedmineConfigurer();
        redmine.setProtocol(protocol);
        return new Validator(redmine, linkUtils);
    }

}
