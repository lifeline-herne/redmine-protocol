package de.psicho.redmine.protocol.dao;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import de.psicho.redmine.protocol.api.JournalHandler;
import de.psicho.redmine.protocol.model.IssueJournalWrapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class IssueDao {

    private static final String TASK = "Aufgabe";
    private static final String TOP = "TOP";
    private static final String AGENDA = "Agenda";
    private static final String ISSUE = "Issue";

    @NonNull
    private final JdbcTemplate jdbcTemplate;

    @NonNull
    private final JournalHandler journalHandler;

    public List<IssueJournalWrapper> findJournals(String tracker, String changeDate) {
        var sql = """
                  SELECT issues.id, issues.subject, issues.assigned_to_id, journals.id, false FROM journals
                  INNER JOIN issues ON journals.journalized_id=issues.id INNER JOIN trackers ON issues.tracker_id = trackers.id
                  WHERE trackers.name = ? AND journals.journalized_type = ?
                  AND journals.notes <> '' AND SUBSTRING(journals.created_on, 1, 10) = ?
                  ORDER BY journals.created_on
                """;

        return jdbcTemplate.query(sql, (rs, rowNum) -> journalHandler.retrieveJournal(rs), tracker, ISSUE, changeDate);
    }

    public List<IssueJournalWrapper> findAgendaIssues() {
        var sql = """
                  SELECT issues.id, issues.subject, issues.assigned_to_id, 0, true FROM issues
                  INNER JOIN trackers ON issues.tracker_id = trackers.id
                  INNER JOIN custom_values cv ON cv.customized_id = issues.id
                  INNER JOIN custom_fields cf on cf.id = cv.custom_field_id
                  WHERE cf.name = ? AND cv.customized_type = ? AND cv.value = '1'
                """;

        return jdbcTemplate.query(sql, (rs, rowNum) -> journalHandler.retrieveJournal(rs), AGENDA, ISSUE);
    }

    public Integer getFirstNonEmptyJournalByIssueId(Integer issueId) {
        var sql = """
                  SELECT journals.id FROM journals INNER JOIN issues ON issues.id=journals.journalized_id
                  WHERE issues.id = ? AND journals.notes <> '' ORDER BY journals.id LIMIT 0,1
                """;

        return jdbcTemplate.queryForObject(sql, Integer.class, issueId);
    }

    public String getMaxNum(String year, String numberFieldName) {
        var sql = """
                  SELECT value FROM custom_values cv INNER JOIN custom_fields cf on cv.custom_field_id = cf.id
                  WHERE cf.name = ? AND SUBSTRING(cv.value, -4, 4) = ? ORDER BY value DESC LIMIT 1
                """;
        List<String> result = jdbcTemplate.queryForList(sql, String.class, numberFieldName, year);
        if (result.isEmpty()) {
            return "0";
        } else {
            return result.getFirst();
        }
    }

    public List<IssueJournalWrapper> findTaskJournals(String isoDate) {
        return findJournals(TASK, isoDate);
    }

    public List<IssueJournalWrapper> findTopJournals(String isoDate) {
        return findJournals(TOP, isoDate);
    }

    public void clearAllMarks() {
        var fieldIdSQL = "SELECT id FROM custom_fields WHERE name = ?";
        var agendaFieldId = jdbcTemplate.queryForObject(fieldIdSQL, Integer.class, AGENDA);
        var clearSQL = "UPDATE custom_values SET value = 0 WHERE redmine.custom_values.custom_field_id = ?";
        jdbcTemplate.update(clearSQL, agendaFieldId);
    }
}
