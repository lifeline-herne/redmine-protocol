package de.psicho.redmine.protocol.service;

import static java.lang.String.format;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClient;

import de.psicho.redmine.protocol.model.Quote;
import de.psicho.redmine.protocol.model.Xkcd;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@Service
@AllArgsConstructor
@Slf4j
public class QuotationService {

    public static final String XKCD_SOURCE = "https://xkcd.com/info.0.json";
    public static final String XKCD_IMAGE_BASE_LINK = "https://xkcd.com/";

    @NonNull
    private final RestClient restClient;

    public Quote getQuote() {
        final var xkcd = restClient.get().uri(XKCD_SOURCE).retrieve().body(Xkcd.class);
        if (xkcd != null) {
            return Quote.builder()
                        .title(xkcd.getTitle())
                        .alt(xkcd.getAlt())
                        .image(download(xkcd.getImg()))
                        .url(getUrl(xkcd))
                        .build();
        } else {
            return null;
        }
    }

    private String getUrl(Xkcd xkcd) {
        return XKCD_IMAGE_BASE_LINK + xkcd.getNum();
    }

    private byte[] download(String imageLocation) {
        var image = new byte[0];
        URL url = null;
        try {
            url = new URL(imageLocation);
        } catch (MalformedURLException ex) {
            log.warn(format("Could not download %s - invalid URL.", imageLocation), ex);
        }
        if (url != null) {
            try (InputStream inputStream = url.openStream()) {
                image = IOUtils.toByteArray(inputStream);
            } catch (IOException ex) {
                log.warn(format("Could not download %s.", imageLocation), ex);
            }
        }
        return image;
    }
}
