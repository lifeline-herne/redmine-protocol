package de.psicho.redmine.protocol.api.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class StatusWrapper {

    @JsonProperty("issue_statuses")
    private List<Status> statuses;
}
