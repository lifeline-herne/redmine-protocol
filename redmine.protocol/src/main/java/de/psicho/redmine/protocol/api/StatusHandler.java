package de.psicho.redmine.protocol.api;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClient;
import org.springframework.web.client.RestClientException;

import de.psicho.redmine.protocol.api.model.Status;
import de.psicho.redmine.protocol.api.model.StatusWrapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class StatusHandler {

    public static final String STATUS_URL = "/issue_statuses.json";

    @NonNull
    private final RestClient restClient;

    private List<Status> statusCache;

    public Integer getStatusByName(String statusName) {
        try {
            if (statusCache == null) {
                final var statusWrapper = restClient.get().uri(STATUS_URL).retrieve().body(StatusWrapper.class);
                statusCache = statusWrapper == null ? null : statusWrapper.getStatuses();
            }
            if (statusCache != null) {
                for (var status : statusCache) {
                    if (statusName.equals(status.getName())) {
                        return status.getId();
                    }
                }
            }
        } catch (RestClientException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
