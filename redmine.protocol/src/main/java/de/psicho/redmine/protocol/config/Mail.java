package de.psicho.redmine.protocol.config;

import lombok.Data;

@Data
public class Mail {

    /**
     * Email address to send the finished protocol to instead of the active redmine users.
     * If not set, the active redmine users will be the recipients.
     */
    private String debugRecipient;

    /**
     * Email subject of the protocol.
     * First and only placeholder will provide the date.
     */
    private String subject;

    /**
     * Email body of the protocol.
     * First placeholder will provide the date.
     * Second placeholder will provide the link to the protocol.
     */
    private String body;
}
