package de.psicho.redmine.protocol.config;

import lombok.Data;

@Data
public class Api {

    /**
     * URL of the Api, normally the url that can be seen from protocol to access the website.
     * From within a container it is different than from outside.
     */
    private String url;

    /**
     * Access key for a redmine user.
     * Log into redmine > My Account > see right side.
     */
    private String accessKey;

    public String getUrl() {
        if (url.endsWith("/")) {
            return url;
        } else {
            return url + "/";
        }
    }

}
