package de.psicho.redmine.protocol.service;

import static java.lang.String.format;

import java.io.File;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Rectangle;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import de.psicho.redmine.iTextile.DocumentCreationException;
import de.psicho.redmine.iTextile.iTextile;
import de.psicho.redmine.iTextile.command.TextProperty;
import de.psicho.redmine.protocol.api.IssueHandler;
import de.psicho.redmine.protocol.api.UserHandler;
import de.psicho.redmine.protocol.api.model.Issue;
import de.psicho.redmine.protocol.api.model.User;
import de.psicho.redmine.protocol.config.Protocol;
import de.psicho.redmine.protocol.config.RedmineConfigurer;
import de.psicho.redmine.protocol.controller.IssueProcessingException;
import de.psicho.redmine.protocol.model.IssueJournalWrapper;
import de.psicho.redmine.protocol.model.Quote;
import de.psicho.redmine.protocol.utils.DateUtils;
import de.psicho.redmine.protocol.utils.LinkUtils;
import jakarta.annotation.PostConstruct;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.java.textilej.parser.markup.textile.TextileDialect;

@Component
@RequiredArgsConstructor
@Slf4j
public class ITextService {

    @NonNull
    private final RedmineConfigurer redmineConfigurer;

    @NonNull
    private final IssueHandler issueHandler;

    private String issueLinkPrefix;

    @NonNull
    private final ProtocolService protocolService;

    @NonNull
    private final LinkUtils linkUtils;

    @NonNull
    private final UserHandler userHandler;

    private Protocol redmineProtocol;

    // TODO autowire as a (stateful --> prototype?) bean; how to inject filename (known at runtime, not compile time)
    // REMARK: if this is prototype, the whole chain must be!
    private iTextile iTextile;

    public void endTable() {
        iTextile.endTable();
    }

    public void finalizeITextile(String footer) throws DocumentCreationException {
        iTextile.setFooter(footer);
        iTextile.createFile();
    }

    public void processStatus(Issue protocol, List<IssueJournalWrapper> statusJournals) {
        if (statusJournals.size() > 0) {
            var statusContent = "*Status vom letzten Protokoll*\r\n"
                + statusJournals.stream().map(this::appendJournal).collect(Collectors.joining("\r\n"));
            statusContent = postProcessContent(statusContent);
            iTextile.addTableRow("---", statusContent,
                protocolService.getProtocolUser(protocol, redmineProtocol.getFields().getModeration()));
        }
    }

    public void processTop(List<IssueJournalWrapper> topJournals) {
        for (IssueJournalWrapper top : topJournals) {
            var number = setIssueLinks("#" + top.getIssueId().toString());
            var title = "*" + top.getIssueSubject().trim() + "*\r\n";
            // adding an additional new line is necessary for potential headers (otherwise Markdown won't recognize them)
            var content = "\r\n";
            if (top.getJournal() != null) {
                content += top.getJournal().getNotes();
            } else {
                var issue = issueHandler.getIssue(top.getIssueId());
                content += issue.getDescription();
            }
            content = postProcessContent(content);
            var assignee = top.getAssignee();
            if (assignee == null || assignee == 0) {
                throw new IssueProcessingException(
                    format("Das Ticket %s wurde niemandem zugewiesen!", linkUtils.getShortLink(top.getIssueId())));
            }
            var responsible = protocolService.getProtocolUser(assignee);
            iTextile.addTableRow(number, title + content, responsible);
        }
    }

    public void startITextile(String filename) {
        var file = new File(filename);
        file.getParentFile().mkdirs();
        iTextile = new iTextile(filename);
    }

    public void startTable(Issue protocol) {
        iTextile.startTable(3);
        iTextile.setTableColumnWidth(0, 45f);
        iTextile.setTableColumnWidth(2, 60f);
        iTextile.setTableColumnParser(0, new TextileDialect());
        iTextile.setTableColumnParser(1, new TextileDialect());

        var format = TextProperty.builder().style(Font.BOLD).build();
        iTextile.setTableHeader(format, BaseColor.GRAY, "Nr.", "TOP / Beschluss", "Verantw.");

        var moderation = protocolService.getProtocolUser(protocol, redmineProtocol.getFields().getModeration());
        var devotion = protocolService.getProtocolUser(protocol, redmineProtocol.getFields().getDevotion());
        iTextile.addTableRow("---", "Moderation / Andacht", moderation + "/ " + devotion);
    }

    public void writeDocumentHeader(Issue protocol) {
        heading();

        var title = "Gemeinderat am " + DateUtils.dateToGer(protocol.getStartDate()) + " bei "
            + protocolService.getProtocolValue(protocol, redmineProtocol.getFields().getLocation());
        iTextile.startTable(2, Rectangle.NO_BORDER);
        iTextile.setTableColumnFormat(0, TextProperty.builder().alignment(Element.ALIGN_LEFT).build());
        iTextile.setTableColumnFormat(1, TextProperty.builder().alignment(Element.ALIGN_RIGHT).build());
        iTextile.setTableColumnWidth(0, 460);
        iTextile.addTableRow(title, protocolService.getProtocolValue(protocol, redmineProtocol.getFields().getNumber()));
        iTextile.endTable();

        paragraph("Essen: " + protocolService.getProtocolValue(protocol, redmineProtocol.getFields().getMeal()));
        paragraph("Anwesend: " + protocolService.getProtocolValue(protocol, redmineProtocol.getFields().getParticipants()));
    }

    public void addDocumentCandy(Quote quote) {
        if (quote != null) {
            paragraph(quote.getTitle());
            iTextile.addParagraph(quote.getImage(), quote.getUrl());
            paragraph(quote.getAlt());
        }
    }

    private String appendJournal(IssueJournalWrapper status) {
        return "#" + status.getIssueId() + " (" + status.getIssueSubject() + ")" + "\r\n" + status.getJournal().getNotes();
    }

    private void heading() {
        var opalBlue = new BaseColor(25, 73, 150);
        iTextile.addParagraph(insertSpaces("PROTOKOLL", 8),
            TextProperty.builder().size(18.0f).style(Font.BOLD).color(opalBlue).alignment(Element.ALIGN_CENTER).build());
    }

    @PostConstruct
    private void init() {
        this.redmineProtocol = redmineConfigurer.getProtocol();
        this.issueLinkPrefix = redmineConfigurer.getIssues().getLink();
    }

    private String insertSpaces(String content, int numberOfSpaces) {
        String spacing = StringUtils.repeat(" ", numberOfSpaces);
        String[] stringArray = content.split("");
        return StringUtils.join(stringArray, spacing);
    }

    private String markPersons(String content) {
        var replaced = content;
        for (User user : userHandler.getAll()) {
            var matcher = Pattern.compile("(" + user.getFirstName() + ")([^a-zA-ZäöüÄÖÜß]|$)").matcher(replaced);
            // dirty hack: don't use html tags - redmine.protocol may not know redmine.iTextile is using html internally
            // sadly, it does not work otherwise
            replaced = matcher.replaceAll("<b>%{background:yellow}$1%</b>$2");
        }
        return replaced;
    }

    private void paragraph(String title) {
        iTextile.addParagraph(title, TextProperty.builder().size(12.0f).style(Font.NORMAL).color(BaseColor.BLACK).build());
    }

    private String postProcessContent(String content) {
        var linked = setIssueLinks(content);
        var unattached = linked.replace("attachment:", "");
        return markPersons(unattached);
    }

    private String setIssueLinks(String content) {
        var matcher = Pattern.compile("#(\\d+)").matcher(content);
        return matcher.replaceAll("\"#$1\":" + issueLinkPrefix + "$1");
    }
}
