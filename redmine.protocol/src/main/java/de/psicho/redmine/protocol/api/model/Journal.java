package de.psicho.redmine.protocol.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Journal {

    private Integer id;

    private String notes;

}
