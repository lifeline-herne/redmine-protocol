package de.psicho.redmine.protocol.api.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAlias;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomFieldsWrapper {

    @JsonAlias("custom_fields")
    private List<CustomField> customFields;
}
