package de.psicho.redmine.protocol.controller;

import static java.lang.String.format;

import static org.apache.commons.lang3.StringUtils.capitalize;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import de.psicho.redmine.protocol.api.IssueHandler;
import de.psicho.redmine.protocol.api.model.Issue;
import de.psicho.redmine.protocol.config.Fields;
import de.psicho.redmine.protocol.config.Protocol;
import de.psicho.redmine.protocol.config.RedmineConfigurer;
import de.psicho.redmine.protocol.model.Validation;
import de.psicho.redmine.protocol.utils.LinkUtils;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class Validator {

    @NonNull
    private final RedmineConfigurer redmineConfigurer;

    @NonNull
    private final LinkUtils linkUtils;

    public void validateIssueId(String issueId) {
        Validation validation = new Validation();

        if (StringUtils.isBlank(issueId)) {
            validation.add("Der Parameter issueId muss angegeben sein.");
        }
        if (!StringUtils.isNumeric(issueId)) {
            validation.add("issueId muss eine Zahl sein.");
        }

        validation.evaluate();
    }

    public void validateProtocol(Issue protocol, Boolean isBeingClosed) {
        Validation validation = new Validation(linkUtils.getShortLink(protocol.getId()));
        Protocol redmineProtocol = redmineConfigurer.getProtocol();

        if (!protocol.getTrackerName().equals(redmineProtocol.getName())) {
            validation.add(format("Das Ticket ist kein Protokoll (Tracker muss '%s' sein).", redmineProtocol.getName()));
            validation.evaluate();
        }

        if (protocol.getStatusName().equals(redmineProtocol.getClosed())) {
            validation.add("Das Protokoll wurde bereits geschlossen.");
        }

        if (protocol.getAssigneeId() == null) {
            validation.add("Das Protokoll wurde niemandem zugewiesen. Es muss dem Protokollschreiber entsprechen.");
        }

        if (protocol.getStartDate() == null) {
            validation.add("'Beginn' muss ein gültiges Datum sein und dem Tag des Meetings entsprechen.");
        }

        validateMandatoryFields(protocol, validation, redmineProtocol.getMandatory());

        if (isBeingClosed) {
            validateMandatoryFields(protocol, validation, redmineProtocol.getMandatoryOnClose());
        }

        validation.evaluate();
    }

    private void validateMandatoryFields(Issue protocol, Validation validation, List<String> mandatoryFields) {
        for (String field : mandatoryFields) {
            final var mappedField = getProtocolFieldByName(field);
            if (mappedField != null && protocol.getCustomFieldByName(mappedField) == null
                    || StringUtils.isBlank(protocol.getCustomFieldByName(mappedField).getValue())) {
                validation.add(format("Feld '%s' muss angegeben werden.", mappedField));
            }
        }
    }

    String getProtocolFieldByName(String configFieldName) {
        final var configFieldsObject = redmineConfigurer.getProtocol().getFields();
        final var configGetter = ReflectionUtils.findMethod(Fields.class, "get" + capitalize(configFieldName));
        try {
            return (String) configGetter.invoke(configFieldsObject);
        } catch (IllegalAccessException | InvocationTargetException | NullPointerException ex) {
            return null;
        }
    }
}
