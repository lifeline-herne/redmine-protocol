package de.psicho.redmine.protocol.api.model;

import java.util.List;

import lombok.Data;

@Data
public class UsersWrapper {

    private List<User> users;
}
