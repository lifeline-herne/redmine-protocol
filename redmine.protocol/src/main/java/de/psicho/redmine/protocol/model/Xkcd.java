package de.psicho.redmine.protocol.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;

@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Xkcd {

    String num;
    String year;
    String month;
    String day;

    String safe_title;
    String title;
    String alt;
    String img;

    String news;
    String transcript;
}
