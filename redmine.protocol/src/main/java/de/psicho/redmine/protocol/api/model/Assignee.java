package de.psicho.redmine.protocol.api.model;

import lombok.Data;

@Data
public class Assignee {

    private Integer id;
}
