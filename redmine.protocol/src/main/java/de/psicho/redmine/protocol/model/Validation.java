package de.psicho.redmine.protocol.model;

import static java.lang.String.format;

import java.util.ArrayList;
import java.util.List;

import de.psicho.redmine.protocol.controller.ValidationException;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class Validation {

    @Getter
    private final List<String> messages = new ArrayList<>();

    private String linkToProtocol;

    public Validation(String linkToProtocol) {
        this.linkToProtocol = linkToProtocol;
    }

    public void add(String msg) {
        messages.add(msg);
    }

    public void add(Validation other) {
        other.getMessages().forEach(this::add);
    }

    public String render() {
        var output = new StringBuilder();

        if (linkToProtocol != null) {
            output.append(format("Fehler für Ticket %s:<br />", linkToProtocol));
        }
        if (messages.size() > 1) {
            output.append("<ol>");
            for (String line : messages) {
                output.append("<li>").append(line).append("</li>");
            }
            output.append("</ol>");
        } else {
            output.append("<br />");
            output.append(messages.get(0));
        }

        return output.toString();
    }

    public void evaluate() {
        if (isNotEmpty()) {
            throw new ValidationException(render());
        }
    }

    private boolean isNotEmpty() {
        return messages.size() > 0;
    }
}
