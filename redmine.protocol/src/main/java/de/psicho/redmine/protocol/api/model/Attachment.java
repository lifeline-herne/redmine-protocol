package de.psicho.redmine.protocol.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Attachment {

    private Integer id;

    private String filename;

    private Integer filesize;

    @JsonProperty("content_type")
    private String contentType;

    @JsonProperty("content_url")
    private String contentUrl;
}
