package de.psicho.redmine.protocol.api;

import static org.apache.commons.lang3.StringUtils.removeEnd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestClient;

import de.psicho.redmine.protocol.config.RedmineConfigurer;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
public class RestClientBean {

    @NonNull
    private final RedmineConfigurer redmineConfigurer;

    @Bean
    public RestClient restClient() {
        final var redmineApi = redmineConfigurer.getApi();
        return RestClient.builder()
                         .baseUrl(removeEnd(redmineApi.getUrl(), "/"))
                         .defaultHeader("X-Redmine-API-Key", redmineApi.getAccessKey())
                         .build();
    }

}
