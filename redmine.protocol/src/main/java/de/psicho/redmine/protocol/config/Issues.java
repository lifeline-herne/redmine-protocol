package de.psicho.redmine.protocol.config;

import lombok.Data;

@Data
public class Issues {

    /**
     * Link to be used when accessing an issue from PDF or Web UI.
     * Host and port part will be different from redmine.api.url if running in a container.
     */
    private String link;

    public String getLink() {
        if (link.endsWith("/")) {
            return link;
        } else {
            return link + "/";
        }
    }
}
