package de.psicho.redmine.protocol.service;

import static java.lang.String.format;
import static java.util.stream.Collectors.toList;

import static de.psicho.redmine.protocol.utils.DateUtils.getYear;
import static org.apache.commons.lang3.StringUtils.EMPTY;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import de.psicho.redmine.protocol.api.CustomFieldHandler;
import de.psicho.redmine.protocol.api.IssueHandler;
import de.psicho.redmine.protocol.api.TrackerHandler;
import de.psicho.redmine.protocol.api.UserHandler;
import de.psicho.redmine.protocol.api.model.Issue;
import de.psicho.redmine.protocol.api.model.User;
import de.psicho.redmine.protocol.config.RedmineConfigurer;
import de.psicho.redmine.protocol.dao.IssueDao;
import de.psicho.redmine.protocol.model.AttachedFile;
import de.psicho.redmine.protocol.model.IssueJournalWrapper;
import de.psicho.redmine.protocol.utils.DateUtils;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class ProtocolService {

    private final static String PROTOCOL_PATH = "results";
    private final static String PROTOCOL_FILE_PREFIX = "Gemeinderat ";
    private final static String PDF_SUFFIX = ".pdf";

    @NonNull
    private final RedmineConfigurer redmineConfigurer;

    @NonNull
    private final UserHandler userHandler;

    @NonNull
    private final IssueDao issueDao;

    @NonNull
    private final CustomFieldHandler customFieldHandler;

    @NonNull
    private final TrackerHandler trackerHandler;

    @NonNull
    private final IssueHandler issueHandler;

    public static String getProtocolFileName(String protocolStartDate) {
        return PROTOCOL_FILE_PREFIX + DateUtils.dateToIso(protocolStartDate) + PDF_SUFFIX;
    }

    public static String getProtocolPath(String protocolStartDate) {
        return PROTOCOL_PATH + "/" + getProtocolFileName(protocolStartDate);
    }

    public String getProtocolValue(Issue protocol, String fieldName) {
        var field = protocol.getCustomFieldByName(fieldName);
        return field.getValue();
    }

    public String getProtocolUser(Issue protocol, String fieldName) {
        var field = protocol.getCustomFieldByName(fieldName);
        var userId = Integer.valueOf(field.getValue());
        return getProtocolUser(userId);
    }

    public String getProtocolUser(Integer userId) {
        var user = userHandler.getUserById(userId);
        if (user == null) {
            throw new RuntimeException(format("Der User mit der Id %d konnte nicht gefunden werden.", userId));
        }
        var name = user.getFirstName();
        if (redmineConfigurer.getProtocol().getSwitches().getPrintLastNameInitial()) {
            name += " " + user.getLastName().charAt(0) + ".";
        }
        return name;
    }

    public List<Issue> getOpenProtocols() {
        return issueHandler.getOpenIssues(getProtocolId());
    }

    public List<Issue> getClosedProtocols() {
        return issueHandler.getClosedIssues(getProtocolId());
    }

    public Set<AttachedFile> getAttachedFiles(List<IssueJournalWrapper> journals) {
        Set<AttachedFile> result = new HashSet<>();
        for (IssueJournalWrapper journal : journals) {
            var content = EMPTY;
            if (journal.getJournal() != null) {
                content += journal.getJournal().getNotes();
            } else {
                var issue = issueHandler.getIssue(journal.getIssueId());
                content += issue.getDescription();
            }
            result.addAll(extractAttachments(content, journal.getIssueId()));
        }
        return result;
    }

    public String[] getRecipients() {
        List<String> recipients = new ArrayList<>();
        var debugRecipient = redmineConfigurer.getMail().getDebugRecipient();
        if (StringUtils.isNotBlank(debugRecipient)) {
            recipients.add(debugRecipient);
        } else {
            List<User> users = userHandler.getAll();
            users.forEach(user -> recipients.add(user.getMail()));
        }
        return recipients.toArray(new String[0]);
    }

    /**
     * Remove all issues from agendaIssues that are already in topJournals and remove the Agenda marks from topJournals (as
     * they're already done).
     *
     * @param agendaIssues issues marked with Agenda
     * @param topJournals issues having a protocol entry
     */
    public List<IssueJournalWrapper> difference(List<IssueJournalWrapper> agendaIssues, List<IssueJournalWrapper> topJournals) {
        topJournals.forEach(issue -> issue.setAgenda(false));
        List<Integer> topIssuesIds = topJournals.stream().map(IssueJournalWrapper::getIssueId).collect(toList());
        return agendaIssues.stream().filter(issue -> !topIssuesIds.contains(issue.getIssueId())).collect(toList());
    }

    public void setNextNumber(Issue protocol) {
        final var nextNumber = getNextNumber(getYear(protocol.getStartDate()));
        protocol.changeCustomField(getIdOfNumberField(), nextNumber);
    }

    private String getNextNumber(String year) {
        final var numberFieldName = redmineConfigurer.getProtocol().getFields().getNumber();
        final var maxNum = issueDao.getMaxNum(year, numberFieldName);
        final var firstOfYear = "01/" + year;
        if (maxNum == null) {
            return firstOfYear;
        }
        final var split = maxNum.split("/");
        if (split.length != 2) {
            return firstOfYear;
        }
        int maxGivenYear;
        try {
            maxGivenYear = Integer.parseInt(split[0]);
        } catch (NumberFormatException e) {
            return firstOfYear;
        }
        return StringUtils.leftPad(String.valueOf(maxGivenYear + 1), 2, "0") + "/" + year;
    }

    private Integer getIdOfNumberField() {
        final var numberFieldName = redmineConfigurer.getProtocol().getFields().getNumber();
        final var customFields = customFieldHandler.getCustomFields();
        for (var field : customFields) {
            if ("issue".equals(field.getType()) && numberFieldName.equals(field.getName())) {
                return field.getId();
            }
        }
        return null;
    }

    private Set<AttachedFile> extractAttachments(String content, Integer issueId) {
        Matcher matcher = Pattern.compile("attachment:\"(.*?)\"").matcher(content);
        Set<AttachedFile> result = new HashSet<>();
        while (matcher.find()) {
            var attachedFileName = matcher.group().replace("\"", "").replace("attachment:", "");
            var attachedFile = AttachedFile.builder().issueId(issueId).fileName(attachedFileName).build();
            result.add(attachedFile);
        }
        return result;
    }

    private Integer getProtocolId() {
        var protocolTracker = redmineConfigurer.getProtocol().getName();
        return trackerHandler.getTrackerIdByName(protocolTracker);
    }
}
