package de.psicho.redmine.protocol.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Upload {

    private String token;

    private String filename;

    @JsonProperty("content_type")
    private String contentType;

}
