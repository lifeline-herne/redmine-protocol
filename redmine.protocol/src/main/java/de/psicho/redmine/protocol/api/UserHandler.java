package de.psicho.redmine.protocol.api;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClient;
import org.springframework.web.client.RestClientException;

import de.psicho.redmine.protocol.api.model.User;
import de.psicho.redmine.protocol.api.model.UsersWrapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class UserHandler {

    // https://www.redmine.org/projects/redmine/wiki/Rest_Users
    // status 1: active
    public static final String USER_URL = "/users.json?status=1";

    @NonNull
    private final RestClient restClient;
    private List<User> usersCache;

    public User getUserById(Integer userId) {
        try {
            retrieveUsersCache();
            if (usersCache != null) {
                for (var user : usersCache) {
                    if (userId.equals(user.getId())) {
                        return user;
                    }
                }
            }
        } catch (RestClientException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public List<User> getAll() {
        try {
            retrieveUsersCache();
            return usersCache;
        } catch (RestClientException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private void retrieveUsersCache() {
        if (usersCache == null) {
            final var usersWrapper = restClient.get().uri(USER_URL).retrieve().body(UsersWrapper.class);
            usersCache = usersWrapper == null ? null : usersWrapper.getUsers();
        }
    }
}
