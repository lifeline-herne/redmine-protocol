package de.psicho.redmine.protocol.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomField {

    private Integer id;

    private String name;

    private String value;

    @JsonProperty("customized_type")
    private String type;

}
