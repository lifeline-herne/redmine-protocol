package de.psicho.redmine.protocol.controller;

import java.util.List;
import java.util.Set;

import de.psicho.redmine.protocol.api.model.Issue;
import de.psicho.redmine.protocol.model.AttachedFile;
import de.psicho.redmine.protocol.model.IssueJournalWrapper;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ResponseInfo {

    private final String issueId;
    private final String isoDate;
    private final List<IssueJournalWrapper> statusJournals;
    private final List<IssueJournalWrapper> topJournals;
    private final Set<AttachedFile> attachedFiles;
    private final Issue issue;
    private final String issueUrl;
}
