package de.psicho.redmine.protocol.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Status {

    private Integer id;

    private String name;

    @JsonProperty("is_closed")
    private Boolean closed;
}
