package de.psicho.redmine.protocol.api;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Component;

import de.psicho.redmine.protocol.api.model.Journal;
import de.psicho.redmine.protocol.model.IssueJournalWrapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class JournalHandler {

    @NonNull
    private final IssueHandler issueHandler;

    public IssueJournalWrapper retrieveJournal(ResultSet rs) {
        IssueJournalWrapper result = new IssueJournalWrapper();

        try {
            var issueId = rs.getInt(1);
            var subject = rs.getString(2);
            var assignee = rs.getInt(3);
            int journalId = rs.getInt(4);
            boolean isAgenda = rs.getBoolean(5);
            var journal = journalId == 0 ? null : getJournal(issueId, journalId);
            result.setIssueId(issueId);
            result.setIssueSubject(subject);
            result.setJournal(journal);
            result.setAssignee(assignee);
            result.setAgenda(isAgenda);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return result;
    }

    private Journal getJournal(Integer issueId, Integer journalId) {
        var issue = issueHandler.getIssueWithJournals(issueId);
        if (issue != null && issue.getJournals() != null) {
            for (var journal : issue.getJournals()) {
                var currentJournalId = journal.getId();
                if (currentJournalId != null && currentJournalId.equals(journalId)) {
                    return journal;
                }
            }
        }
        return null;
    }
}
