package de.psicho.redmine.protocol.api;

import static java.lang.String.format;
import static java.net.URLDecoder.decode;
import static java.nio.charset.StandardCharsets.UTF_8;

import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM_VALUE;
import static org.springframework.http.MediaType.APPLICATION_PDF_VALUE;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClient;

import de.psicho.redmine.protocol.api.model.Issue;
import de.psicho.redmine.protocol.api.model.Upload;
import de.psicho.redmine.protocol.api.model.UploadWrapper;
import de.psicho.redmine.protocol.controller.IssueProcessingException;
import de.psicho.redmine.protocol.utils.LinkUtils;
import lombok.AllArgsConstructor;
import lombok.NonNull;

@Component
@AllArgsConstructor
public class AttachmentHandler {

    private static final String UPLOAD_URL = "/uploads.json?filename={filename}";

    @NonNull
    private final IssueHandler issueHandler;

    @NonNull
    private final RestClient restClient;

    @NonNull
    private final LinkUtils linkUtils;

    public byte[] getAttachment(Integer issueId, String attachmentFileName) {
        final var attachments = issueHandler.getIssueWithAttachments(issueId).getAttachments();
        final var found = attachments.stream()
                                     .filter(attachment -> attachmentFileName.equals(attachment.getFilename()))
                                     .findFirst()
                                     .orElse(null);
        if (found == null) {
            throw new IssueProcessingException(format("Für das Ticket %s wurde der Anhang '%s' nicht gefunden.",
                linkUtils.getShortLink(issueId), attachmentFileName));
        } else {
            final var downloadUrl = decode(found.getContentUrl(), UTF_8);
            return restClient.get().uri(downloadUrl).retrieve().body(byte[].class);
        }
    }

    public void addAttachment(Issue issue, File attachment) {
        byte[] file = readFile(attachment);
        if (file != null) {
            final UploadWrapper uploadWrapper = uploadAttachment(file, attachment.getName());
            addUploadToIssue(issue, uploadWrapper.getUpload().getToken(), attachment.getName());
        }
    }

    private byte[] readFile(File attachment) {
        try (var fileInputStream = new FileInputStream(attachment)) {
            return IOUtils.toByteArray(fileInputStream);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private void addUploadToIssue(Issue issue, String token, String fileName) {
        Upload upload = new Upload();
        upload.setToken(token);
        upload.setContentType(APPLICATION_PDF_VALUE);
        upload.setFilename(fileName);
        if (issue.getUploads() == null) {
            issue.setUploads(new ArrayList<>());
        }
        issue.getUploads().add(upload);
    }

    private UploadWrapper uploadAttachment(byte[] file, String fileName) {
        return restClient.post()
                         .uri(UPLOAD_URL, fileName)
                         .header("Content-Type", APPLICATION_OCTET_STREAM_VALUE)
                         .body(file)
                         .retrieve()
                         .body(UploadWrapper.class);
    }
}
