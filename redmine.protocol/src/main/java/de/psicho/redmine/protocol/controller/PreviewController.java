package de.psicho.redmine.protocol.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import de.psicho.redmine.protocol.api.IssueHandler;
import de.psicho.redmine.protocol.config.RedmineConfigurer;
import de.psicho.redmine.protocol.dao.IssueDao;
import de.psicho.redmine.protocol.service.ProtocolService;
import de.psicho.redmine.protocol.utils.DateUtils;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Controller
@RequiredArgsConstructor
public class PreviewController {

    @NonNull
    private final IssueHandler issueHandler;
    @NonNull
    private final IssueDao issueDao;
    @NonNull
    private final ProtocolService protocolService;
    @NonNull
    private final RedmineConfigurer redmineConfigurer;

    @GetMapping("/protocol/{issueId}/previewHTML")
    public String previewHTML(@PathVariable String issueId, Model model) {
        var protocol = issueHandler.getIssue(issueId);
        var isoDate = DateUtils.dateToIso(protocol.getStartDate());
        var statusJournals = issueDao.findTaskJournals(isoDate);
        var topJournals = issueDao.findTopJournals(isoDate);
        var attachedFiles = protocolService.getAttachedFiles(topJournals);

        ResponseInfo responseInfo = ResponseInfo.builder()
                                                .issueId(issueId)
                                                .isoDate(isoDate)
                                                .statusJournals(statusJournals)
                                                .topJournals(topJournals)
                                                .attachedFiles(attachedFiles)
                                                .issue(protocol)
                                                .issueUrl(redmineConfigurer.getIssues().getLink())
                                                .build();

        model.addAttribute("responseInfo", responseInfo);
        return "preview-html";
    }
}
