package de.psicho.redmine.protocol.utils;

import org.apache.commons.lang3.StringUtils;

public class DateUtils {

    public static String dateToIso(String date) {
        return date.substring(0, 10);
    }

    public static String dateToGer(String date) {
        return zeroPad(getDay(date)) + "." + zeroPad(getMonth(date)) + "." + getYear(date);
    }

    public static String getYear(String date) {
        return date.substring(0, 4);
    }

    public static String getMonth(String date) {
        return date.substring(5, 7);
    }

    public static String getDay(String date) {
        return date.substring(8, 10);
    }

    public static String zeroPad(String input) {
        return StringUtils.leftPad(input, 2, "0");
    }
}
