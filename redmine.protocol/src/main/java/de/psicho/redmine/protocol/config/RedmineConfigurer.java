package de.psicho.redmine.protocol.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Data
@Configuration
@ConfigurationProperties(prefix = "redmine")
@EnableConfigurationProperties
public class RedmineConfigurer {

    /**
     * Mail properties.
     */
    @NestedConfigurationProperty
    private Mail mail;

    /**
     * Protocol properties.
     */
    @NestedConfigurationProperty
    private Protocol protocol;

    /**
     * Redmine api properties.
     */
    @NestedConfigurationProperty
    private Api api;

    /**
     * Issue properties.
     */
    @NestedConfigurationProperty
    private Issues issues;
}
