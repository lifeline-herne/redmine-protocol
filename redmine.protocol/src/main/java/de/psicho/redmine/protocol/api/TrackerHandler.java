package de.psicho.redmine.protocol.api;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClient;
import org.springframework.web.client.RestClientException;

import de.psicho.redmine.protocol.api.model.Tracker;
import de.psicho.redmine.protocol.api.model.TrackersWrapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class TrackerHandler {

    public static final String TRACKER_URL = "/trackers.json";

    @NonNull
    private final RestClient restClient;
    private List<Tracker> trackersCache;

    public Integer getTrackerIdByName(String trackerName) {
        try {
            if (trackersCache == null) {
                final var trackersWrapper = restClient.get().uri(TRACKER_URL).retrieve().body(TrackersWrapper.class);
                trackersCache = trackersWrapper == null ? null : trackersWrapper.getTrackers();
            }
            if (trackersCache != null) {
                for (var tracker : trackersCache) {
                    if (trackerName.equals(tracker.getName())) {
                        return tracker.getId();
                    }
                }
            }
        } catch (RestClientException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
