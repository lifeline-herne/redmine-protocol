package de.psicho.redmine.protocol.api.model;

import lombok.Data;

@Data
public class UploadWrapper {

    private Upload upload;
}
