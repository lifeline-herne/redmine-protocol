package de.psicho.redmine.protocol.config;

import lombok.Data;

@Data
public class Fields {

    /**
     * Field name holding participants of the meeting.
     */
    private String participants;
    /**
     * Field name holding responsible person or persons for meal.
     */
    private String meal;

    /**
     * Field name holding location of the meeting.
     */
    private String location;

    /**
     * Field name holding person responsible for moderation
     */
    private String moderation;

    /**
     * Field name holding person responsible for devotion.
     */
    private String devotion;

    /**
     * Field name holding the number of the meeting.
     */
    private String number;
}
