package de.psicho.redmine.protocol.controller;

public class IssueProcessingException extends RuntimeException {

    private static final long serialVersionUID = -7804383503788732798L;

    public IssueProcessingException(String message) {
        super(message);
    }

}
