package de.psicho.redmine.protocol.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Quote {

    String title;
    String alt;
    byte[] image;
    String url;
}
