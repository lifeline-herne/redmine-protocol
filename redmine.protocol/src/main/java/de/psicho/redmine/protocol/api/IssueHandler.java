package de.psicho.redmine.protocol.api;

import static java.lang.String.format;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClient;
import org.springframework.web.client.RestClientException;

import de.psicho.redmine.protocol.api.model.Issue;
import de.psicho.redmine.protocol.api.model.IssueWrapper;
import de.psicho.redmine.protocol.api.model.IssuesWrapper;
import de.psicho.redmine.protocol.controller.ValidationException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class IssueHandler {

    public static final String INCLUDE_ATTACHMENTS = "?include=attachments";
    public static final String INCLUDE_JOURNALS = "?include=journals";
    public static final String ISSUE_URL = "/issues/{issueId}.json";
    public static final String ISSUES_URL = "/issues.json?tracker_id={trackerId}&status_id={status}&sort=id:desc";
    public static final String CLOSED = "closed";
    public static final String OPEN = "open";

    @NonNull
    final RestClient restClient;

    public Issue getIssue(Integer issueId) {
        return getIssue(issueId, ISSUE_URL);
    }

    public Issue getIssue(String issueId) {
        int ticketId = Integer.parseInt(issueId);
        Issue issue = getIssue(ticketId, ISSUE_URL);
        if (issue == null) {
            throw new ValidationException(format("Ticket '%s' konnte nicht gefunden werden.", issueId));
        } else {
            return issue;
        }
    }

    public Issue getIssueWithJournals(Integer issueId) {
        return getIssue(issueId, ISSUE_URL + INCLUDE_JOURNALS);
    }

    public Issue getIssueWithAttachments(Integer issueId) {
        return getIssue(issueId, ISSUE_URL + INCLUDE_ATTACHMENTS);
    }

    public void updateIssue(Issue issue) {
        // @formatter:off
        restClient.put()
                  .uri(ISSUE_URL, issue.getId())
                  .body(new IssueWrapper(issue))
                  .retrieve();
        // @formatter:on
    }

    public List<Issue> getOpenIssues(Integer trackerId) {
        return getIssues(trackerId, OPEN);
    }

    public List<Issue> getClosedIssues(Integer trackerId) {
        return getIssues(trackerId, CLOSED);
    }

    private List<Issue> getIssues(Integer trackerId, String status) {
        try {
            final var issuesWrapper = restClient.get().uri(ISSUES_URL, trackerId, status).retrieve().body(IssuesWrapper.class);
            return issuesWrapper == null ? null : issuesWrapper.getIssues();
        } catch (RestClientException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private Issue getIssue(Integer issueId, String url) {
        try {
            final var issueWrapper = restClient.get().uri(url, issueId).retrieve().body(IssueWrapper.class);
            return issueWrapper == null ? null : issueWrapper.getIssue();
        } catch (RestClientException ex) {
            ex.printStackTrace();
            return null;
        }
    }

}
