package de.psicho.redmine.protocol.api.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Issue {

    private Integer id;

    private Tracker tracker;

    private Status status;

    @JsonProperty("status_id")
    private Integer statusId;

    private String subject;

    private String description;

    @JsonProperty("start_date")
    private String startDate;

    @JsonProperty("assigned_to")
    private Assignee assignee;

    @JsonProperty("custom_fields")
    private List<CustomField> customFields;

    private List<Journal> journals;

    private List<Attachment> attachments;

    private List<Upload> uploads;

    public Integer getAssigneeId() {
        return assignee == null ? null : assignee.getId();
    }

    public String getStatusName() {
        return status == null ? null : status.getName();
    }

    public String getTrackerName() {
        return tracker == null ? null : tracker.getName();
    }

    public CustomField getCustomFieldByName(String name) {
        return this.customFields.stream().filter(customField -> name.equals(customField.getName())).findFirst().orElse(null);
    }

    public void changeCustomField(Integer customFieldId, String value) {
        for (var field : customFields) {
            if (field.getId().equals(customFieldId)) {
                field.setValue(value);
            }
        }
    }
}
