package de.psicho.redmine.protocol.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.context.properties.NestedConfigurationProperty;

import lombok.Data;

@Data
public class Protocol {

    /**
     * List of mandatory field names.
     * Use field names according to keys in application configuration.
     */
    private List<String> mandatory = new ArrayList<>();

    /**
     * List of additional mandatory field names when closing the protocol.
     * Use field names according to keys in application configuration.
     */
    private List<String> mandatoryOnClose = new ArrayList<>();

    /**
     * Tracker type of protocol issues in redmine.
     */
    private String name;

    /**
     * Redmine status name for closed protocols.
     */
    private String closed;

    /**
     * Properties for field names in redmine.
     */
    @NestedConfigurationProperty
    private Fields fields;

    /**
     * Properties for switches in the application.
     */
    @NestedConfigurationProperty
    private Switches switches;

    /**
     * Application version
     */
    private String version;
}
