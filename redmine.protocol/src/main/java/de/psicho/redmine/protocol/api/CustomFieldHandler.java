package de.psicho.redmine.protocol.api;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClient;
import org.springframework.web.client.RestClientException;

import de.psicho.redmine.protocol.api.model.CustomField;
import de.psicho.redmine.protocol.api.model.CustomFieldsWrapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class CustomFieldHandler {

    public static final String CUSTOM_FIELD_URL = "/custom_fields.json";

    @NonNull
    private final RestClient restClient;

    private List<CustomField> customFieldsCache;

    public List<CustomField> getCustomFields() {
        if (customFieldsCache != null) {
            return customFieldsCache;
        }
        try {
            final var customFields = restClient.get().uri(CUSTOM_FIELD_URL).retrieve().body(CustomFieldsWrapper.class);
            customFieldsCache = customFields == null ? null : customFields.getCustomFields();
            return customFieldsCache;
        } catch (RestClientException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
