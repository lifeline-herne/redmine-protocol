package de.psicho.redmine.protocol.controller;

import static java.lang.String.format;
import static java.util.stream.Collectors.toList;

import static de.psicho.redmine.protocol.service.ProtocolService.getProtocolFileName;
import static de.psicho.redmine.protocol.service.ProtocolService.getProtocolPath;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM_VALUE;
import static org.springframework.http.MediaType.APPLICATION_PDF_VALUE;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import de.psicho.redmine.iTextile.utils.ResourceUtils;
import de.psicho.redmine.protocol.api.AttachmentHandler;
import de.psicho.redmine.protocol.api.IssueHandler;
import de.psicho.redmine.protocol.api.StatusHandler;
import de.psicho.redmine.protocol.api.model.Issue;
import de.psicho.redmine.protocol.config.Protocol;
import de.psicho.redmine.protocol.config.RedmineConfigurer;
import de.psicho.redmine.protocol.dao.IssueDao;
import de.psicho.redmine.protocol.model.AttachedFile;
import de.psicho.redmine.protocol.model.IssueJournalWrapper;
import de.psicho.redmine.protocol.service.ITextService;
import de.psicho.redmine.protocol.service.ProtocolService;
import de.psicho.redmine.protocol.service.QuotationService;
import de.psicho.redmine.protocol.utils.DateUtils;
import de.psicho.redmine.protocol.utils.LinkUtils;
import jakarta.annotation.PostConstruct;
import jakarta.mail.Address;
import jakarta.mail.Message;
import jakarta.mail.MessagingException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequiredArgsConstructor
public class ProtocolController {

    private static final String AUTOCLOSE_TRUE = "?autoclose=1";
    private static final String FOOTER_LINK = "https://vipc.de/proto";
    private static final String FOOTER_TEMPLATE = "Generiert mit Code von %s in v%s. Nutzt iText, lizensiert unter AGPL 3.0.";

    @NonNull
    private final ProtocolService protocolService;
    @NonNull
    private final JavaMailSender mailSender;
    @NonNull
    private final AttachmentHandler attachmentHandler;
    @NonNull
    private final IssueHandler issueHandler;
    @NonNull
    private final StatusHandler statusHandler;
    @NonNull
    private final IssueDao issueDao;
    @NonNull
    private final RedmineConfigurer redmineConfigurer;
    @NonNull
    private final Validator validator;
    @NonNull
    private final ITextService iTextService;
    @NonNull
    private final LinkUtils linkUtils;
    @NonNull
    private final Environment environment;
    @NonNull
    private final QuotationService quotationService;

    private Protocol redmineProtocol;
    private String footer;
    private String linkFooter;

    @PostConstruct
    private void init() {
        redmineProtocol = redmineConfigurer.getProtocol();
        footer = format(FOOTER_TEMPLATE, FOOTER_LINK, redmineProtocol.getVersion());
        linkFooter = format(FOOTER_TEMPLATE, format("<a href='%1$s'>%1$s</a>", FOOTER_LINK), redmineProtocol.getVersion());
    }

    @GetMapping({ "/", "/protocol" })
    public String info() {
        var output = new StringBuilder();
        final var openProtocols = protocolService.getOpenProtocols();
        final var openProtocolIds = openProtocols.stream().map(Issue::getId).toList();
        if (openProtocolIds.isEmpty()) {
            output.append("Kein offenes Protokoll vorhanden.<br />");
        } else {
            for (var openProtocolId : openProtocolIds) {
                output.append("<a href=\"")
                      .append(pathToProtocol(openProtocolId))
                      .append("\">Öffne Protokoll #")
                      .append(openProtocolId)
                      .append("</a><br />");
            }
        }
        output.append("<br /><a href='/closed'>Geschlossene Protokolle anzeigen</a>");
        return wrapHtml(output.toString());
    }

    @GetMapping("/closed")
    private String showClosedProtocols() {
        var closedProtocols = protocolService.getClosedProtocols();
        var result = new StringBuilder("<strong>Vorhandene Protokolle:</strong>");
        for (var issue : closedProtocols) {
            result.append("<br />").append(linkUtils.getShortLink(issue.getId())).append(": ");
            appendProtocolPreview(result, issue);
        }
        return wrapHtml(result.toString());
    }

    @GetMapping("/protocol/{issueId}")
    public String createProtocol(@PathVariable String issueId,
        @RequestParam(name = "autoclose", defaultValue = "false") boolean autoclose) {

        ResponseInfo responseInfo = null;
        Exception exception = null;

        try {
            validator.validateIssueId(issueId);
            var protocol = issueHandler.getIssue(issueId);
            validator.validateProtocol(protocol, false);

            var protocolStartDate = protocol.getStartDate();
            var isoDate = DateUtils.dateToIso(protocolStartDate);

            protocolService.setNextNumber(protocol);

            iTextService.startITextile(getProtocolPath(protocolStartDate));
            iTextService.writeDocumentHeader(protocol);
            iTextService.startTable(protocol);

            var statusJournals = issueDao.findTaskJournals(isoDate);
            iTextService.processStatus(protocol, statusJournals);

            var topJournals = issueDao.findTopJournals(isoDate);
            var agendaIssues = protocolService.difference(issueDao.findAgendaIssues(), topJournals);
            addDescription(topJournals);
            iTextService.processTop(topJournals);
            var attachedFiles = protocolService.getAttachedFiles(topJournals);
            var allTopIssues = Stream.concat(topJournals.stream(), agendaIssues.stream()).collect(toList());

            iTextService.endTable();
            iTextService.addDocumentCandy(quotationService.getQuote());
            iTextService.finalizeITextile(footer);

            if (autoclose) {
                closeProtocol(protocol);
                sendProtocol(protocol, attachedFiles);
            }

            responseInfo = ResponseInfo.builder()
                                       .issueId(issueId)
                                       .isoDate(isoDate)
                                       .statusJournals(statusJournals)
                                       .topJournals(allTopIssues)
                                       .attachedFiles(attachedFiles)
                                       .issue(protocol)
                                       .build();
        } catch (ValidationException | IssueProcessingException ex) {
            exception = ex;
            log.error(ex.getMessage());
        } catch (Exception ex) {
            exception = ex;
            log.error(ExceptionUtils.getStackTrace(ex));
        }

        return createResponse(responseInfo, exception, autoclose);
    }

    @GetMapping(value = "/protocol/{issueId}/previewPDF", produces = APPLICATION_PDF_VALUE)
    public ResponseEntity<byte[]> previewProtocol(@PathVariable String issueId) throws IOException {
        var ticketId = Integer.parseInt(issueId);
        var protocol = issueHandler.getIssue(ticketId);
        var protocolStartDate = protocol.getStartDate();

        var pdfFile = new File(getProtocolPath(protocolStartDate));
        var fileSystemResource = new FileSystemResource(pdfFile);
        var inputStream = fileSystemResource.getInputStream();
        byte[] pdf = IOUtils.toByteArray(inputStream);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=" + getProtocolFileName(protocolStartDate));

        return ResponseEntity.ok().headers(headers).body(pdf);
    }

    @GetMapping(value = "/download/{issueId}/{fileName}.{extension}", produces = APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<byte[]> downloadAttachment(@PathVariable Integer issueId, @PathVariable String fileName,
        @PathVariable String extension) {

        var fullFilename = fileName + "." + extension;
        var binaryAttachment = attachmentHandler.getAttachment(issueId, fullFilename);

        var headers = new HttpHeaders();
        headers.setContentDispositionFormData("attachment", fullFilename);

        return ResponseEntity.ok().headers(headers).body(binaryAttachment);
    }

    private void appendProtocolPreview(StringBuilder result, Issue issue) {
        boolean fileExists = protocolFileExistsLocally(issue.getId());
        result.append(issue.getSubject()).append(" ");
        result.append("<a href=\"").append(pathToProtocol(issue.getId())).append("/previewHTML").append("\">HTML</a>");
        if (fileExists) {
            result.append(" | <a href=\"").append(pathToProtocol(issue.getId())).append("/previewPDF").append("\">PDF</a>");
        }
    }

    private boolean protocolFileExistsLocally(Integer ticketId) {
        var protocol = issueHandler.getIssue(ticketId);
        var protocolStartDate = protocol.getStartDate();
        var pdfFile = new File(getProtocolPath(protocolStartDate));
        var fileSystemResource = new FileSystemResource(pdfFile);
        return fileSystemResource.exists();
    }

    private String pathToProtocol(Integer protocolId) {
        var path = getCurrentHost();
        path = appendPath(path, "protocol");
        path = appendPath(path, protocolId.toString());
        return path;
    }

    private String appendPath(String path, String suffix) {
        var result = path;
        if (!path.endsWith("/")) {
            result += "/";
        }
        return result + suffix;
    }

    private void addDescription(List<IssueJournalWrapper> topJournals) {
        for (IssueJournalWrapper topJournal : topJournals) {
            var firstJournalId = issueDao.getFirstNonEmptyJournalByIssueId(topJournal.getIssueId());
            var journal = topJournal.getJournal();
            if (journal.getId().equals(firstJournalId)) {
                var issue = issueHandler.getIssue(topJournal.getIssueId());
                var description = issue.getDescription();
                if (StringUtils.isNotBlank(description)) {
                    journal.setNotes(description + "\r\n\r\n<hr/>\r\n\r\n" + journal.getNotes());
                }
            }
        }
    }

    private String createResponse(ResponseInfo responseInfo, Exception thrownException, boolean closed) {
        var result = EMPTY;

        if (thrownException instanceof ValidationException || thrownException instanceof IssueProcessingException) {
            result += thrownException.getMessage();
        } else if (thrownException != null) {
            result += format("<pre>%s</pre>", ExceptionUtils.getStackTrace(thrownException));
        } else {
            final var ticketLink = linkUtils.getShortLink(Integer.parseInt(responseInfo.getIssueId()));
            final var ticketDate = DateUtils.dateToGer(responseInfo.getIsoDate());
            final var protocolNumber = getNumber(responseInfo.getIssue());
            if (closed) {
                result += format("Protokoll %s für Ticket #%s am %s wurde erstellt, verschickt und <strong>geschlossen</strong>.",
                    protocolNumber, ticketLink, ticketDate);
            } else {
                result += format("Erzeuge Protokoll %s für Ticket #%s am %s", protocolNumber, ticketLink, ticketDate);
            }
            if (responseInfo.getStatusJournals() != null && !responseInfo.getStatusJournals().isEmpty()) {
                final var statuses = augmentIssueInfo(responseInfo.getStatusJournals(), responseInfo.getAttachedFiles());
                result += format("<br/><h3>Status: %s</h3> %s", responseInfo.getStatusJournals().size(), statuses);
            }
            final var topSize = responseInfo.getTopJournals().size();
            final var journals = augmentIssueInfo(responseInfo.getTopJournals(), responseInfo.getAttachedFiles());
            result += format("<br/><h3>TOP: %s</h3> %s<p><a href=\"%s/previewPDF\">PDF Vorschau</a></p>", topSize, journals,
                getCurrentPath());
            if (!closed) {
                final var closePath = getCurrentPath() + AUTOCLOSE_TRUE;
                result += format("<br/>Protokoll <a href=\"%s\"><strong>schließen</strong></a>.", closePath);
            }
        }

        return wrapHtml(result);
    }

    private String getCurrentPath() {
        ServletUriComponentsBuilder builder = ServletUriComponentsBuilder.fromCurrentRequestUri();
        builder.replaceQuery(null);
        return builder.build().toString();
    }

    private String getCurrentHost() {
        ServletUriComponentsBuilder builder = ServletUriComponentsBuilder.fromCurrentRequestUri();
        if (builder.build().getPort() == 443) {
            builder.port(null);
        }
        builder.replaceQuery(null);
        builder.replacePath(null);
        return builder.build().toString();
    }

    private StringBuilder augmentIssueInfo(List<IssueJournalWrapper> issueJournals, Set<AttachedFile> attachedFiles) {
        var result = new StringBuilder();
        for (IssueJournalWrapper issueJournal : issueJournals) {
            var issueId = issueJournal.getIssueId();
            result.append("<br/>");
            if (!issueJournal.getAgenda()) {
                result.append("<del>");
            }
            result.append(linkUtils.getShortLink(issueId)).append(": ").append(issueJournal.getIssueSubject());
            if (!issueJournal.getAgenda()) {
                result.append("</del>");
            }

            var filteredFiles =
                attachedFiles.stream().filter(file -> file.getIssueId().equals(issueId)).collect(Collectors.toSet());
            if (!filteredFiles.isEmpty()) {
                result.append("<ul>");
                try {
                    var uri = new URI(getCurrentPath());
                    for (AttachedFile file : filteredFiles) {
                        var fileName = file.getFileName();
                        result.append("<li><a href=\"")
                              .append(uri.getScheme())
                              .append("://")
                              .append(uri.getAuthority())
                              .append("/download/")
                              .append(issueId)
                              .append("/")
                              .append(fileName)
                              .append("\">")
                              .append(fileName)
                              .append("</a>");
                    }
                } catch (URISyntaxException ex) {
                    ex.printStackTrace();
                }
                result.append("</ul>");
            }
        }
        return result;
    }

    private void closeProtocol(Issue protocol) {
        validator.validateProtocol(protocol, true);
        final var protocolStartDate = protocol.getStartDate();
        final var subject = "Gemeinderat " + getNumber(protocol) + " am " + DateUtils.dateToGer(protocolStartDate);
        protocol.setSubject(subject);
        protocol.setStatusId(statusHandler.getStatusByName(redmineProtocol.getClosed()));
        final var attachment = new File(getProtocolPath(protocolStartDate));
        attachmentHandler.addAttachment(protocol, attachment);
        issueDao.clearAllMarks();
        issueHandler.updateIssue(protocol);
    }

    private String getNumber(Issue protocol) {
        final var numberField = redmineConfigurer.getProtocol().getFields().getNumber();
        return protocol.getCustomFieldByName(numberField).getValue();
    }

    private void sendProtocol(Issue protocol, Set<AttachedFile> attachedFiles) {
        var mailConfig = redmineConfigurer.getMail();

        var protocolStartDate = protocol.getStartDate();
        var message = mailSender.createMimeMessage();
        var file = new FileSystemResource(new File(getProtocolPath(protocolStartDate)));

        try {
            var dateGer = DateUtils.dateToGer(protocolStartDate);
            var linkToProtocol = linkUtils.getLongLink(protocol.getId());
            var helper = new MimeMessageHelper(message, true);
            helper.setTo(protocolService.getRecipients());
            helper.setSubject(format(mailConfig.getSubject(), dateGer));
            helper.setText(format("<html><body>" + mailConfig.getBody() + "</body></html>", dateGer, linkToProtocol), true);
            helper.addAttachment(getProtocolFileName(protocolStartDate), file);

            for (AttachedFile attachedFile : attachedFiles) {
                var binaryAttachment = attachmentHandler.getAttachment(attachedFile.getIssueId(), attachedFile.getFileName());
                var byteArrayResource = new ByteArrayResource(binaryAttachment);
                helper.addAttachment(attachedFile.getFileName(), byteArrayResource);
            }
        } catch (MessagingException ex) {
            ex.printStackTrace();
        }

        try {
            var recipients = Arrays.stream(message.getRecipients(Message.RecipientType.TO))
                                   .map(Address::toString)
                                   .collect(Collectors.joining(", "));
            log.info(format("Sending %s to %s.", message.getSubject(), recipients));
        } catch (MessagingException ex) {
            ex.printStackTrace();
        }

        mailSender.send(message);
    }

    private String wrapHtml(String body) {
        var style = ResourceUtils.readResource("protocol.style.css");
        final var home = "<a href=\"/\"><img src=\"/home.jpg\" width=\"32\"></a> ";
        final var profiles =
            "<strong><font color=\"red\">Verwende Konfiguration " + String.join(", ", environment.getActiveProfiles())
                + "</font></strong>" + " (Empfänger: " + String.join(",", protocolService.getRecipients()) + ")" + "<hr />";
        body = home + profiles + body;
        return format("<!DOCTYPE html><html><head><style>%s</style></head><body>%s</body><footer>%s</footer></html>", style, body,
            linkFooter);
    }
}
