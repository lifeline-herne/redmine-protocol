package de.psicho.redmine.protocol.model;

import de.psicho.redmine.protocol.api.model.Journal;
import lombok.Data;

@Data
public class IssueJournalWrapper {

    Integer issueId;
    String issueSubject;
    Journal journal;
    Integer assignee;
    Boolean agenda;
}
