package de.psicho.redmine.protocol.config;

import lombok.Data;

@Data
public class Switches {

    /**
     * True if person's names shall be printed as Firstname L.
     * False if person's names shall be printed as Firstname
     */
    private Boolean printLastNameInitial;
}
