package de.psicho.redmine.protocol.api.model;

import lombok.Data;

@Data
public class Tracker {

    private Integer id;
    private String name;
}
