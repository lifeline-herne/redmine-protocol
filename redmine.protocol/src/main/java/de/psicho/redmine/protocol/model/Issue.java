package de.psicho.redmine.protocol.model;

import lombok.Data;

@Data
public class Issue {

    Integer id;
    String subject;
}
